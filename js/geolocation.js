/*
var locations = [
	['title', lat, long, infoWindow_string],
];
*/
locations = [];

function findDomainLocation() {

	var _this = $('#btnFindDomainLocation'),
		domain_url = $('#domain_url'),
		warning = $('#domain-validation-warning');

	if ( !isUrlValid( domain_url.val() ) ) {
		domain_url.focus();
		warning.html('This is not a valid domain.').removeClass('hidden');

		_this.closest('.form-group').addClass('has-error');

	} else {

		_this.attr('disabled','disabled').addClass('disabled').find('.fa').removeClass('hidden');
		_this.closest('.form-group').removeClass('has-error');
		warning.html('').addClass('hidden');

		var hostInfos = 'http://ip-api.com/json/' + domain_url.val();
		$.getJSON( hostInfos )
		.done(function(data) {

			if ( data.status != 'fail' ) {

				removeItemFromMap('domain');

				var infowindow_domain = GoogleMapsWindowInfo('domain',data);

				domain_location = ['Domain', data.lat, data.lon, infowindow_domain, 'domain'];
				_this.removeAttr('disabled').removeClass('disabled').find('.fa').addClass('hidden');

				setMarkers(domain_location);

			} else {
				domain_url.focus();
				warning.html('Domain not found.').removeClass('hidden');
				_this.closest('.form-group').addClass('has-error');
			}

			_this.removeAttr('disabled').removeClass('disabled').find('.fa').addClass('hidden');

		});

	}

}

function isUrlValid(userInput) {
	var urlregex = /^(www\.)?[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,40}(:[0-9]{1,40})?(\/.*)?$/;
	return urlregex.test(userInput);
}

function setMarkers(new_locations) {

	current_locations = getMarkers();
	locations.push( new_locations );

	showMap();

}

function getMarkers() {
	return locations;

}

function showMap() {

	var locations = getMarkers();

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 2,
		// center: new google.maps.LatLng(locations[0][1],locations[0][2]),
		center: new google.maps.LatLng(0,0),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false
	});

	var marker, i;

	for ( i=0;i<locations.length;i++ ) {

		var infowindow = new google.maps.InfoWindow({
			// content: locations[i][3]
			content: '<div>'+
				'<h1 class="firstHeading">' + locations[i][0] + '</h1>'+
				'<div>'+
					locations[i][3] +
				'</div>'+
			'</div>'
		});

		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map
		});

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infowindow.open(map, marker);
			}
		})(marker, i));

		map.setCenter(marker.getPosition());

	}

	$('#map').removeClass('hidden');

}

function updateLocationDetails(data){
	var now = new Date();

	$("#location_query").html(data.query);
	$("#location_country").html(data.country);
	$("#location_regionName").html(data.regionName);
	$("#location_city").html(data.city);
	$("#location_timezone").html(data.timezone);
	$("#location_lat").html(data.lat);
	$("#location_lon").html(data.lon);

	$("table").removeClass("empty");
	$(".help").click(function(e){
		var fieldName = $(e.currentTarget).closest('tr').find('.field_name').text();
		alert("This is your " + fieldName + " from ISP " + data.isp + " at " + now);
	});

}

function GoogleMapsWindowInfo(window_type,arrayInfos) {

	var infos =
		'<div>'+
			// '<h1 class="firstHeading">' + ( window_type == 'me' ? 'I am here' : 'Domain Infos' ) + '.</h1>'+
			'<div>'+
				'<table class="table table-striped">' +
					( arrayInfos.as 			!= undefined ? '<tr><td>as</td><td>' + arrayInfos.as + '</td></tr>' 					: '' ) +
					( arrayInfos.city 			!= undefined ? '<tr><td>city</td><td>' + arrayInfos.city + '</td></tr>' 				: '' ) +
					( arrayInfos.country 		!= undefined ? '<tr><td>country</td><td>' + arrayInfos.country + '</td></tr>' 			: '' ) +
					( arrayInfos.countryCode 	!= undefined ? '<tr><td>countryCode</td><td>' + arrayInfos.countryCode + '</td></tr>'	: '' ) +
					( arrayInfos.isp 			!= undefined ? '<tr><td>isp</td><td>' + arrayInfos.isp + '</td></tr>' 					: '' ) +
					( arrayInfos.lat 			!= undefined ? '<tr><td>lat</td><td>' + arrayInfos.lat + '</td></tr>' 					: '' ) +
					( arrayInfos.lon 			!= undefined ? '<tr><td>lon</td><td>' + arrayInfos.lon + '</td></tr>' 					: '' ) +
					( arrayInfos.org 			!= undefined ? '<tr><td>org</td><td>' + arrayInfos.org + '</td></tr>' 					: '' ) +
					( arrayInfos.query 			!= undefined ? '<tr><td>query</td><td>' + arrayInfos.query + '</td></tr>' 				: '' ) +
					( arrayInfos.region 		!= undefined ? '<tr><td>region</td><td>' + arrayInfos.region + '</td></tr>' 			: '' ) +
					( arrayInfos.regionName 	!= undefined ? '<tr><td>regionName</td><td>' + arrayInfos.regionName + '</td></tr>' 	: '' ) +
					( arrayInfos.status 		!= undefined ? '<tr><td>status</td><td>' + arrayInfos.status + '</td></tr>' 			: '' ) +
					( arrayInfos.timezone 		!= undefined ? '<tr><td>timezone</td><td>' + arrayInfos.timezone + '</td></tr>' 		: '' ) +
					( arrayInfos.zip 			!= undefined ? '<tr><td>zip</td><td>' + arrayInfos.zip + '</td></tr>' 					: '' ) +
				'</table>' +
			'</div>'+
		'</div>';

	return infos;

}

function getMyAddress() {

	$.ajax({
		type : 'GET',
		url : 'http://ip-api.com/json/',
		complete: function(data) {
			if ( data.status == 'success' )
				return data;
			else
				return false;
		}
	});

}

function getMyLocation() {

	var _this = $('#btnMyLocation');

	_this.attr('disabled','disabled').addClass('disabled').find('.fa').removeClass('hidden');

	$.ajax({
		type : 'GET',
		url : 'http://ip-api.com/json/',
		success : function(response){
			updateLocationDetails(response);

			var my_location_infowindow = GoogleMapsWindowInfo('me',response);
			var my_location_map_marker = ['I\'m here', response.lat, response.lon, my_location_infowindow, 'me'];

			setMarkers(my_location_map_marker);
		},
		complete: function() {
			_this.removeAttr('disabled').removeClass('disabled').find('.fa').addClass('hidden');
		}
	});
}

function resetLocationDetails() {
	updateLocationDetails({
		query: "0.0.0.0",
		country: "",
		regionName: "",
		city: "",
		timezone: "",
		lat: "",
		lon: ""
	});
	$("table").addClass("empty");

	removeItemFromMap('me');

}

function removeItemFromMap(removedItem) {

	var current_locations = getMarkers();
	var total_locations = current_locations.length;

	for ( i=0;i<total_locations;++i ) {

		var me_index = current_locations[i].indexOf( removedItem );
		if ( me_index > 0 )
			current_locations[i].splice(current_locations[me_index], 1);

	}

	showMap();

}

function initializePage(){
	window.indexTemplate = $('#index').html();
	window.locationTemplate = $('#locationInfo').html();

	window.indexTemplate = Handlebars.compile(window.indexTemplate);
	window.locationTemplate = Handlebars.compile(window.locationTemplate);

	$("#mainContent").html(window.indexTemplate());
	$("#geoLocationContainer").html(window.locationTemplate({
		id: 0,
		query: "0.0.0.0",
		country: "",
		regionName: "",
		city: "",
		timezone: "",
		lat: "",
		lon: ""
	}));
}

$(document).ready(function(){
	initializePage();
});
