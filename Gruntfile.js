module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask("default", ["cssmin","uglify","watch"]);

	grunt.initConfig({

		cssmin: {
			combine: {
				files: {
					'css/ac-test.min.css': [
						'assets/bootstrap/dist/css/bootstrap.min.css',
						'css/geolocation.css'
					]
				}
			}
		},

		uglify: {
			options: {
				mangle: false
			},
			js: {
				files: {
					'js/ac-test.min.js': [
						'assets/jquery-1.10.2.min.js',
						'assets/handlebars-v1.3.0.js',
						'assets/bootstrap/dist/js/bootstrap.min.js',
						'js/geolocation.js'
					],

				}
			}

		},

		watch: {
			scripts: {
				files: ['css/*.css','js/*.js'],
				tasks: ["cssmin","uglify"]
			},
			options: {
				debounceDelay: 1,
			}
		}

	});

};
