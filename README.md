# Avenue Code UI Challenge - Part I #

### About the app ###
* As a web surfer, you are able to locate both you and any website on a map.
* You can use a desktop, tablet or a cell phone.

### How to use ###
* Just open the /index.html file in your desktop, tablet or mobile browser.
* Click on "My location". A map will be displayed with a marker where you are.
* Follow the "Domain URL" field to get a physical location of some domain.
* You can reset your location clicking at "Reset location". Your marker will be removed from the map but it keeps the marker of the domain, if some has passed before.
* You can see a map with 1 or 2 markers in these cases: you, domain, you+domain.

### How to test ###
* The tests were write using Jasmine.
* Open the file /tests/spec/index.html in your desktop, tablet or mobile browser.
* If the tests passed, you will see a green bar, otherwise, a red bar.

### Tools ###
* NPM: base
* Bower: dependencies manager
* Grunt: tasks automations
* Bootstrap: layout
* FontAwesome: icons
* jQuery: javascript framework
* Jasmine: javascript test runner
