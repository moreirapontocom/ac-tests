describe('Testing...', function() {

	it('Validating domain URL', function() {

		var res = isUrlValid('avenuecode.com');

		expect(res).not.toBe(null);
		expect(res).toBe(true);
	});

	it('Finding my address', function() {

		var res = getMyAddress();

		expect(res).not.toBe(null);
		expect(true).toBe(true);
	});

});
